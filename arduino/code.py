import board
import time
import neopixel
import digitalio
import adafruit_scd4x
import adafruit_ssd1306
import adafruit_ds3231
from adafruit_bme280 import basic as adafruit_bme280

# Initialize I2C bus and devices
i2c = board.I2C()
scd = adafruit_scd4x.SCD4X(i2c, address=0x62)
display = adafruit_ssd1306.SSD1306_I2C(128, 32, i2c)

rtc = adafruit_ds3231.DS3231(i2c)
bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c, address=0x76)
bme280.sea_level_pressure = 1013.4 # Setting a standard sea level

deg = 'F'
YELLOW = (255, 150, 0)
print_interval = 60

LOG_DATA = '/co2_data.txt'

def _format_time(datetime):
    return "{:02}/{:02} {:02}:{:02}:{:02}".format(
        datetime.tm_mon,
        datetime.tm_mday,
        datetime.tm_hour,
        datetime.tm_min,
        datetime.tm_sec,
    )

def _ftime(datetime):
    return "{:02}:{:02}".format(
        datetime.tm_hour,
        datetime.tm_min,
    )

def to_f(C):
    if deg == 'F':
        temp = (C * 9/5) + 32
        return temp
    return C

pixel = neopixel.NeoPixel(board.NEOPIXEL, 1)
pixel.brightness = 0.1

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT
led.value = True

def set_pixel(co2):
    if co2 in range(200, 400):
        percent = ((co2 - 200)/400)
        green_color = (255*percent)
        GREEN = (0, green_color, 0)
        pixel.fill(GREEN)

    if co2 in range(401, 1000):
        percent = ((co2 - 400)/600)
        blue_color = (255*percent)
        BLUE = (0, 0, blue_color)
        pixel.fill(BLUE)

    if co2 in range(1001, 3000):
        percent = ((co2 - 1000)/3000)
        pixel.brightness = percent
        pixel.fill(YELLOW)

    if co2 in range(3001, 5000):
        percent = ((co2 - 3000)/5000)
        red_color = (255*percent)
        RED = (red_color, 0, 0)
        pixel.brightness = percent
        pixel.fill(RED)


# Put sensor in basic periodic mode
scd.start_periodic_measurement()

# Put sensor in low-power mode
# scd.start_low_periodic_measurement()

# delay = 29.2 # Low periodic

delay = 5.2  # Periodic Measure

print("\nStarting up...\n")

avg_data = [1] * 6
co2_display_position = (2, 0)  # (x, y) position of CO2 value on display

# Initialize variables for plotting
plot_data = []
max_value = 5000  # If you have more thann 5000 CO2 then I'm sorry to hear that

# PID

dtime = time.time()

last = None
now = None

def log_periodically(string):
    global last, now
    now = time.time()
    if last is None or now - last >= print_interval:
        try:
            with open(LOG_DATA, 'a', encoding="utf8") as ruin_flash:
                ruin_flash.write(string + '\n')
                ruin_flash.flush()
                last = now
        except OSError as e:
            delay = 0.5
            if e.args[0] == 28:
                delay = 0.25
            led.value = not led.value
            time.sleep(delay)
            


# Main loop
while True:
    if scd.data_ready:
        since = time.time() - dtime
        dtime = time.time()
        pressure = int(bme280.pressure)
        scd.set_ambient_pressure(pressure)
        set_pixel(scd.CO2)
        co2 = scd.CO2
        temp = "{:.1f}°{}".format(to_f(scd.temperature), deg)
        otemp = "{:.1f}°{}".format(to_f(bme280.temperature), deg)
        # print("Delay: {}s, {}".format(delay, since))

        current_time = _format_time(rtc.datetime)

        nice_output = "{}, {} CO2, {}, {:.1f}% {:.2f} hPa, ~{:.2f} m".format(current_time, co2, otemp, bme280.relative_humidity, bme280.pressure, bme280.altitude)
        output = "{},{},{},{:.1f},{:.2f},{:.2f}".format(current_time, co2, otemp, bme280.relative_humidity, bme280.pressure, bme280.altitude)

        print(output)
        log_periodically(output)
        plot_data.append(co2)

        # For Mu plotting
        # print("({},{},{},{})".format(co2, temp, scd.relative_humidity, delay))
        # print("({}, {})".format(delay, since))

        avg_data.pop(0)
        avg_data.append(sum(plot_data[-6:]) / 6)

        trend = 0
        if avg_data[-1] > avg_data[-2]:
            trend = 1
        elif avg_data[-1] < avg_data[-2]:
            trend = -1

        # Update trend label and arrow
        if trend == 1:
            trend_label = "Up"
        elif trend == -1:
            trend_label = "Down"
        else:
            trend_label = "Stable"

        my_time = _ftime(rtc.datetime)
        if len(plot_data) > 128:
            plot_data.pop(0)  # Remove oldest data point
        display.fill(0)  # Clear display
        for i in range(len(plot_data)):
            x = i
            y = 32 - (plot_data[i] * 32 // max_value)
            display.pixel(x, y, 1)
        display.text("CO2: {} Time: {}".format(co2, my_time), co2_display_position[0], co2_display_position[1], 1)
        display.show()  # Update display
    else:
        time.sleep(delay)
