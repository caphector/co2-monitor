## CO2 Monitor and logging device

This is a simple DIY CO2 monitor; it uses an [Arduino RP2040](https://www.seeedstudio.com/XIAO-RP2040-v1-0-p-5026.html), a [SCD40 or SCD41 sensor](https://www.aliexpress.com/w/wholesale-scd40.html) module; the modules are very similar with the SCD41 having better accuracy at higher PPMs, and a [SSD1306 i2c display](https://www.aliexpress.com/w/wholesale-SSD1306-display.html). It also uses a BME280 pressure / temp sensor, an RTC, and a SSD1306 display to log and display the current atmospheric CO2 levels. The pressure sensor is used to correct the CO2 sensor for current ambient pressure. If we could verify current sea-level pressure it can be used for altitude as well with something like `bme280.sea_level_pressure = 1013.4` to set the mean sea level average pressure, but this number will drift as pressure changes.

Once the components are connected (The sensor and display both communicate over i2c and work with 3.3v power) the `code.py` can be loaded. The device will need to have [CircuitPython](https://circuitpython.org/downloads) installed along with libraries `neopixel`, `adafruit_1306`, `adafruit_scd4x`, `adafruit_pixbuf`, `adafruit_framebuf`, `adafruit_displayio_ssd1306`, `adafruit_display_text`, and `adafruit_bus_device`.

At this time the device outputs the current CO2 roughly every 20 seconds and outputs that to the display. The LED on the RP2040 will change color depending on CO2 levels; green is good, blue is "normal indoor air", over 1000 PPM but under 3000 is yellow, and anything over 3000 is red.

Logging data requires writing to /; this requires remounting the drive as read-write. In normal USB mode, this means USB is read-only when the device is in logging mode. A mass storage device might help with this, but this version doesn't have a mass storage device.

Once all libraries and code are installed, install boot.py and restart the device. Note that to exit this mode the serial interface needs to be used via Mu Editor or something similar.

If you connect power to the +5V and GND pins then it should not be connected to USB when the battery is connected

The data is logged CSV with the following columns:

time, CO2 (SCD40), tempo, Humidity (BME), Pressure (BME), Altitude (BME, LOL))

To revert, you need to use the serial interface and issue commands:
 import os
 os.rename("/boot.py", "/boot.bak")
Reboot the device again and it will be USB-writable.
If the device is full then it will blink 4x/second
Other write errors will blink it twice a second

This can be used to set the time
`# Year, Month, Day, Hr, min, sec, weekday, yearday (unused), dst (unused)`
`# t = time.struct_time((2022,2,27,18,30,0,1,-1,-1))`
`# print("Setting time to:", t)`
`# rtc.datetime = t`

Note it will re-set the time on each boot so don't leave it enabled in write-only mode.

![Co2 monitor on a breadboard with display](https://caphector.com/images/projects/co2.png)

![First assembled device](https://caphector.com/images/projects/co2-2.jpeg)

![Running on USB-C](https://caphector.com/images/projects/co2-3.jpeg)
